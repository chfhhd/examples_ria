JQuery getJSON
==============

A simple JQuery Ajax request example using the $.getJSON method.

the $.getJSON function is a shorthand function, equivalent to:

This is a shorthand Ajax function, which is equivalent to:

$.ajax({
  url: url,
  dataType: 'json',
  data: data,
  success: callback
});

See http://api.jquery.com/category/ajax/ for the full suite of low level and high level JQuery Ajax functions.