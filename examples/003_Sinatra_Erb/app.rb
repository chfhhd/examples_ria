require 'rubygems'
require 'sinatra'

enable :sessions

get '/' do
  erb :index
end

get '/edit' do
  erb :form
end

post '/edit' do
  session['reminder_title']       = params['reminder_title']
  session['reminder_description'] = params['reminder_description']

  redirect '/'
end
