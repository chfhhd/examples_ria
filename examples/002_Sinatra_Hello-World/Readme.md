A Sinatra Hello-World Example
=============================

Sinatra is an Internal Domain Specific Language (internal DSL, compare [1]) that allows quickly creating web applications in Ruby.

To run the example install the needed Sinatra Gem first:

	gem install sinatra

Then start your application:

	ruby myapp.rb

This will run your app using the internal server "WEBrick". The server listens on port 4567. If you like to change this speciy another port using the -p parameter.

Routes in Sinatra are HTTP methods that combined with an URL-matching pattern. Each route is defined in a block:

	get '/' do
	 # show something ...
	end

	post '/' do
	  # create something ...
	end

	put '/' do
	  # replace something ...
	end

	patch '/' do
	  # modify something ...
	end

	delete '/' do
	  # annihilate something ...
	end

	options '/' do
	  # appease something ...
	end

Example route pattern for matching e.g. "/hi/foo" and "/hi/bar". The params hash holds the named parameters.

	get '/hi/:name' do
	  # params[:name] is e.g. 'foo' or 'bar'
	  "Hello #{params[:name]}!" 
	end

For more details on Sinatra see the official introduction [2].

---

[1] http://martinfowler.com/bliki/DomainSpecificLanguage.html

[2] http://www.sinatrarb.com/intro.html