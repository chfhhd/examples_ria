Simple Sinatra Erb example
==========================

Erb templating
--------------

This primitive example shows how to use Erb templating in Sinatra.

Placeholder variables can be embedded in the Erb HTML Templates, e.g.:

    <h1><%= post.title %></h1>

ERB recognizes the following placeholder tags.

      <% %>      ... Ruby code, inline with output
      <%= %>     ... Ruby expression, replaced with result
      <%# %>     ... Comment, ignored
      %          ... A line of Ruby code. Treated as <% line %>
      %%         ... Replaced with % if first thing on a line and % processing is used
      <%% or %%> ... Replaced with <% or %> respectively

Sinatra's session handling
--------------------------

The example also shows how to use Sinatra's session implementation. 

Sinatra uses a cookie based session handling by default. This means the session data is stored in the cookie! To improve security in Sinatra's session mechanism, the session data is encrypted by Sinatra by default.