Building a Static-Website using Rack
====================================

"Rack provides a minimal, modular and adaptable interface for developing web applications in Ruby. By wrapping HTTP requests and responses in the simplest way possible, it unifies and distills the API for web servers, web frameworks, and software in between (the so-called middleware) into a single method call.".

(From the Rack documentation website, http://rack.rubyforge.org/doc/)

A minimalistic example
----------------------

	# config.ru
	run Proc.new {|env| [200, {"Content-Type" => "text/html"}, ["Hello, World!"]]}

Save the file as "config.ru" and run the command "rackup". This runs the script with the defaut web server. You can change the webserver with the -s option, e.g. "thin" or "webrick". Standard port is 9292. You can change this using the -P option. See "rackup --help" for more details.

You can test the script with curl, e.g. curl http://127.0.0.1:9292 -v


Serving local files
-------------------


Gemfile

	source :rubygems
	gem 'rack'


The “Gemfile” defines our dependencies. In this case only the Rack framework. Call “bundle install” to resolve the dependency. A file "Gemfile.lock" will be created.


config.ru

	require "rubygems"
	require "bundler"

	# A route for "/date"
	map "/date" do
	  run lambda { |env|
	    headers = {
	      "Content-Type"  => "text/html",
	      "Cache-Control" => "public, max-age=86400"
	    }

	    [200, headers, [DateTime.now.to_s]]
	  }
	end

	# A route for "/" to server static files of the "public" folder.
	# If the file is not available, index.html will be served.
	map "/" do
	  use Rack::Static, root: "public"

	  run lambda { |env|
	    headers = {
	      "Content-Type"  => "text/html",
	      "Cache-Control" => "public, max-age=86400"
	    }

	    body = File.open('public/index.html', File::RDONLY).read
	    [200, headers, [body]]
	  }
	end

This simple script defines two routes. On "/date" the current date will be returned. The other route shows how to serve static files with Rack. If the requested file was found the script will return it in a default HTTP header. If not the next statements are evaluated. This example will then return the content of the "public/index.html" file and a manual defined HTTP header.
