class Contact
  include DataMapper::Resource

  property :id,         Serial
  property :firstname,  String
  property :lastname,   String  
  property :created_at, DateTime
  property :updated_at, DateTime
end