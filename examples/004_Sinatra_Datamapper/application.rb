require 'rubygems'
require 'data_mapper'
require 'sinatra/base'
require 'json'

require './models/contact'

class Application < Sinatra::Base
  DataMapper.setup(:default, "sqlite3://#{Dir.pwd}/db/database.sqlite")
  DataMapper.auto_upgrade!

  before do
    content_type "application/json", :charset => "utf-8"
  end

  # Create new
  post '/contacts' do
    @contact = Contact.create(
        :firstname  => params[:firstname],
        :lastname   => params[:lastname],
        :created_at => Time.now,
        :updated_at => Time.now        
    )
    if @contact.save
      response["location"] = "http://" + request.host + "/contacts/" + @contact.id.to_s
    else
      status 500
    end
  end

  # Read all  
  get '/contacts' do
    @all_contacts = Contact.all
    if @all_contacts.size == 0
      status 404
    else
      @all_contacts.to_json
    end
  end  

  # Read by id
  get '/contacts/:id' do  
    @contact = Contact.get(params[:id])
    if @contact.nil?
      status 404
    else
      @contact.to_json
    end
  end

  # Update by id
  put '/contacts/:id' do
    @contact = Contact.get(params[:id])
    if @contact.nil?
      status 404
    else
      @contact.firstname  = params[:firstname] if not params[:firstname].nil?
      @contact.lastname   = params[:lastname]  if not params[:lastname].nil?  
      @contact.updated_at = Time.now
      if @contact.save
        @contact.to_json
      else
        status 500
      end
    end
  end

  # Delete by id
  delete '/contacts/:id' do
    @contact = Contact.get(params[:id])
    if @contact.nil?
      status 404
    else
      @contact.destroy
    end
  end

  # Start the server if the Ruby file was executed directly
  run! if app_file == $0
end
