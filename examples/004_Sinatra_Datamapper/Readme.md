Sinatra and Datamapper.org example
==================================

The example descripbes how Datamapper.org can be used in principle with Sinatra. Note that this is a just for demonstration. This API example is neither RESTful nor secure or complete.

Use Curl to test the API on the command line:

Store a new contact:

    curl -d "firstname=foo&lastname=bar" http://127.0.0.1:9292/contacts

Get the contact:

    curl http://127.0.0.1:9292/contacts/1

This will return:

    {"id":1,"firstname":"foo","lastname":"bar","created_at":"2012-11-22T13:35:43+01:00"}