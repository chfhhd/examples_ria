require "rubygems"
require "bundler"

# A route for "/date"
map "/date" do
  run lambda { |env|
    headers = {
      "Content-Type"  => "text/html",
      "Cache-Control" => "public, max-age=86400"
    }

    [200, headers, [DateTime.now.to_s]]
  }
end

# A route for "/" to server static files of the "public" folder.
# If the file is not available, index.html will be served.
map "/" do
  use Rack::Static, root: "public"

  run lambda { |env|
    headers = {
      "Content-Type"  => "text/html",
      "Cache-Control" => "public, max-age=86400"
    }

    body = File.open('public/index.html', File::RDONLY).read
    [200, headers, [body]]
  }
end